import React from 'react';
import './App.css';
import './styles/tailwind.css'
import Leaderboard from './components/Leaderboard.js';
import NewTutorial from './components/NewTutorial.js';
import Tutorial from './components/Tutorial.js';
import Libraries from './components/Libraries.js';
import Library from './components/Library.js';
// import Register from './components/Register.js';
import Manage from './components/admin/Manage.js';
import Home from './components/Home.js';
import * as actions from './store/reducers/actions/index.js';
import {connect} from 'react-redux';
import {history} from './history';
import CortexLogo from './images/CortexLogo.png';
import {Transition} from '@headlessui/react'
import {AcademicCapIcon, BookmarkIcon, CogIcon, HomeIcon, MenuAlt2Icon} from '@heroicons/react/outline'
import {SearchIcon} from '@heroicons/react/solid'

import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

const sidebarNavigation = [
    {name: 'Home', href: '/', icon: HomeIcon, current: true},
    {name: 'Learn', href: '/learn', icon: AcademicCapIcon, current: false},
    {name: 'My Stash', href: '/stash', icon: BookmarkIcon, current: false},
    {name: 'Settings', href: '/settings', icon: CogIcon, current: false},
]
const userNavigation = [
    {name: 'Your Profile', href: '#'},
    {name: 'Sign out', href: '#'},
]

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentUser: undefined,
            showProfileDropdown: false
        }
        this.profileClick = this.profileClick.bind(this);
    }

    profileClick() {
        if (this.state.showProfileDropdown) {
            this.setState({
                showProfileDropdown: false
            })
        } else {
            this.setState({
                showProfileDropdown: true
            })
        }
    }

    componentWillMount() {
        const user = this.props.user;

        if (user) {
            this.setState({
                currentUser: user,
            });
        }
    }

    render() {
        return (
            <Router history={history}>
                <div className="App">

                    <div className="h-screen flex">
                        {/* Narrow sidebar */}
                        <div className="hidden w-28 bg-blue-800 overflow-y-auto md:block">
                            <div className="w-full py-6 flex flex-col items-center">
                                <div className="flex-shrink-0 flex items-center">
                                    <img
                                        className="h-12 w-auto"
                                        src={CortexLogo}
                                        alt="Cortex"
                                    />
                                </div>
                                <div className="flex-1 mt-6 w-full px-2 space-y-1">
                                    {sidebarNavigation.map((item) => (
                                        <a
                                            key={item.name}
                                            href={item.href}
                                            className={classNames(
                                                item.current ? 'bg-blue-900 text-white' : 'text-indigo-100 hover:bg-blue-900 hover:text-white',
                                                'group w-full p-3 rounded-md flex flex-col items-center text-xs font-medium'
                                            )}
                                            aria-current={item.current ? 'page' : undefined}
                                        >
                                            <item.icon
                                                className={classNames(
                                                    item.current ? 'text-white' : 'text-indigo-300 group-hover:text-white',
                                                    'h-6 w-6'
                                                )}
                                                aria-hidden="true"
                                            />
                                            <span className="mt-2">{item.name}</span>
                                        </a>
                                    ))}
                                </div>
                            </div>
                        </div>

                        {/* Content area */}
                        <div className="flex-1 flex flex-col">
                            <header className="w-full">
                                <div
                                    className="relative z-10 flex-shrink-0 h-16 bg-white border-b border-gray-200 shadow-sm flex">
                                    <button
                                        type="button"
                                        className="border-r border-gray-200 px-4 text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-blue-500 md:hidden"
                                        onClick={() => this.state.setMobileMenuOpen(true)}>
                                        <span className="sr-only">Open sidebar</span>
                                        <MenuAlt2Icon className="h-6 w-6" aria-hidden="true"/>
                                    </button>
                                    <div className="flex-1 flex justify-between px-4 sm:px-6">
                                        <div className="flex-1 flex">
                                            <div className="relative w-full text-gray-400 focus-within:text-gray-600">
                                                <p className="text-xl py-4">simpson.dev</p>
                                            </div>
                                        </div>
                                        <div className="ml-2 flex items-center space-x-4 sm:ml-6 sm:space-x-6">
                                            <div className="ml-4 flex items-center md:ml-6">

                                                {
                                                    (!this.state.currentUser) ?
                                                        <>
                                                            <div className="space-x-2">
                                                                <a href="/login" type="button"
                                                                   class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md text-blue-700 bg-blue-100 hover:bg-blue-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
                                                                    Log in
                                                                </a>
                                                                <a href="/register" type="button"
                                                                   class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md text-white bg-blue-800 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
                                                                    Register
                                                                </a>
                                                            </div>
                                                        </>
                                                        :
                                                        <>
                                                            <button
                                                                class="p-1 rounded-full text-black hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-blue-600 focus:ring-white">
                                                                <span class="sr-only">View notifications</span>
                                                                <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg"
                                                                     fill="none" viewBox="0 0 24 24"
                                                                     stroke="currentColor" aria-hidden="true">
                                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                                          stroke-width="2"
                                                                          d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"/>
                                                                </svg>
                                                            </button>

                                                            <div class="ml-3 relative">
                                                                <div>
                                                                    <button onClick={this.profileClick}
                                                                            class="max-w-lg bg-pink-600 rounded-full flex items-center text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-pink-600 focus:ring-white"
                                                                            id="user-menu" aria-haspopup="true">
                                                                        <span class="sr-only">Open user menu</span>
                                                                        <div class="h-10 w-10 rounded-full">
                                                                            <div
                                                                                className="text-center text-white py-3 font-mono">0
                                                                            </div>
                                                                        </div>
                                                                    </button>
                                                                </div>

                                                                <Transition
                                                                    show={this.state.showProfileDropdown}
                                                                    enter="transition ease-out duration-100"
                                                                    enterFrom="transform opacity-0 scale-95"
                                                                    enterTo="transform opacity-100 scale-100"
                                                                    leave="transition ease-in duration-75"
                                                                    leaveFrom="transform opacity-100 scale-100"
                                                                    leaveTo="transform opacity-0 scale-95"
                                                                >
                                                                    <div
                                                                        class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5"
                                                                        role="menu" aria-orientation="vertical"
                                                                        aria-labelledby="user-menu">
                                                                        <div className="px-4 py-3 border-b">
                                                                            <p className="text-sm leading-5">Signed in
                                                                                as</p>
                                                                            <p className="text-sm font-medium leading-5 text-gray-900 truncate">
                                                                                {this.state.currentUser.name}
                                                                            </p>
                                                                        </div>
                                                                        <a href="#"
                                                                           class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                                                           role="menuitem">Profile</a>
                                                                        <a href="#"
                                                                           class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                                                           role="menuitem">Settings</a>
                                                                        <a onClick={this.props.logout} href="#"
                                                                           class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                                                           role="menuitem">Logout</a>
                                                                    </div>
                                                                </Transition>
                                                            </div>
                                                        </>
                                                }

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </header>

                            {/* Main content */}
                            <div className="flex-1 flex items-stretch">
                                <main className="flex-1">
                                    {/* Primary column */}
                                    <section
                                        aria-labelledby="primary-heading"
                                        className="min-w-0 flex-1 h-full flex flex-col lg:order-last"
                                    >
                                        <h1 id="primary-heading" className="sr-only">
                                            Photos
                                        </h1>
                                        <main>
                                            <div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
                                                <div class="px-4 py-4 sm:px-0">
                                                    <div>
                                                        <Switch>
                                                            <Route path="/manage">
                                                                <Manage></Manage>
                                                            </Route>
                                                            <Route path="/leaderboard">
                                                                <Leaderboard></Leaderboard>
                                                            </Route>
                                                            <Route path="/libraries/java">
                                                                <Library></Library>
                                                            </Route>
                                                            <Route path="/tutorials/new">
                                                                <NewTutorial></NewTutorial>
                                                            </Route>
                                                            <Route path="/tutorials/:id">
                                                                <Tutorial></Tutorial>
                                                            </Route>
                                                            <Route path="/learn">
                                                                <Libraries></Libraries>
                                                            </Route>
                                                            {/*<Route path="/register">*/}
                                                            {/*  <Register></Register>*/}
                                                            {/*</Route>*/}
                                                            {/*<Route path="/login">*/}
                                                            {/*  <Login></Login>*/}
                                                            {/*</Route>*/}
                                                            <Route path="/">
                                                                <Leaderboard/>
                                                            </Route>
                                                        </Switch>
                                                    </div>
                                                </div>
                                            </div>
                                        </main>
                                    </section>
                                </main>

                                {/* Secondary column (hidden on smaller screens) */}
                                {/*<aside*/}
                                {/*    className="hidden w-96 bg-white border-l border-gray-200 overflow-y-auto lg:block">*/}
                                {/*    /!* Your content *!/*/}
                                {/*</aside>*/}
                            </div>
                        </div>
                    </div>


                    {/* <Navbar currentUser={this.state.currentUser}></Navbar> */}

                </div>
            </Router>
        );
    }

}

function mapStateToProps(state) {
    const user = state.user;
    console.log(state);
    return {
        user,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        // dispatching plain actions
        logout: () => dispatch(actions.logout()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);