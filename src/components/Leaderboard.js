import React from 'react';
import axios from 'axios';
import { ArrowNarrowLeftIcon, ArrowNarrowRightIcon } from '@heroicons/react/solid';

function LeaderListing (props) {
    return (
        <tr>
            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                <span className="px-2 inline-flex text-lg leading-5 font-semibold">
                    {props.index}
                </span>
            </td>
            <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                <span className="px-2 inline-flex text-lg leading-5 font-semibold">
                    {props.member.name}
                </span>
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
                <span>
                    {props.member.points}
                </span>
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
                <span>
                    {props.member.messagesSent}
                </span>
            </td>
        </tr>
    );
}

class Leaderboard extends React.Component {
    
    state = {
        members: [],
        page: 0,
        lastPage: -1,
    }

    constructor(props){
        super(props);
    }

    componentDidMount() {
        this.fetchRankings();
    }

    fetchRankings(){
        axios.get('http://localhost:8080/api/members/leaderboard')
        .then(response => {
            // handle success
            this.setState({
                members: response.data['content'],
                page: response.data['number'],
                lastPage: (response.data['totalPages'] - 1)
            })
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    render(){

        const members = this.state.members.map((member, index) => {
            return <LeaderListing member={member} key={member.id} index={++index}/>
        });

        return (
            <div>
                <div>
                    <div className="mx-auto text-center p-6 space-y-1">
                        <h1 className="text-black font-bold text-6xl">Leaderboard</h1>
                        <p className="font-semibold text-2xl text-gray-700">These are the member rankings for the <a className="text-2xl text-blue-600 font-bold" href="https://discord.gg/cortexdev">Cortex Development</a> Discord community. </p>
                    </div>
                </div>

                <div className="bg-gradient-to-br from-blue-600 to-gray-700 rounded-2xl shadow-xl max-w-7xl mx-auto">
                    <div className="flex flex-col px-12 py-10 max-w-6xl m-auto">
                        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                            <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                                <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-t-lg">
                                    <table className="min-w-full divide-y divide-gray-200">
                                        <thead>
                                        <tr>
                                            <th scope="col" className="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                #
                                            </th>
                                            <th scope="col" className="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Discord Name
                                            </th>
                                            <th scope="col" className="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Points
                                            </th>
                                            <th scope="col" className="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Messages Sent
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody className="bg-white divide-y divide-gray-200">
                                        
                                        {members}
                                        
                                        </tbody>
                                    </table>
                                </div>
                                <Example currentPage={this.state.page + 1} lastPage={this.state.lastPage}></Example>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ) 
    }

}

function Example(props) {
    return (
      <nav className="border-t border-gray-200 px-4 flex items-center justify-between sm:px-0 bg-white pb-3 rounded-b-lg">
        <div className="-mt-px w-0 flex-1 flex">
          <a
            href="#"
            className="border-t-2 border-transparent pl-20 pt-4 pr-1 inline-flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300"
          >
            <ArrowNarrowLeftIcon className="mr-3 h-5 w-5 text-gray-400" aria-hidden="true" />
            Previous
          </a>
        </div>
        <div className="hidden md:-mt-px md:flex">
          {/* Current: "border-indigo-500 text-indigo-600", Default: "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300" */}
          <a
            href="#"
            className="border-indigo-500 text-indigo-600 border-t-2 pt-4 px-4 inline-flex items-center text-sm font-medium"
            aria-current="page"
          >
            {props.currentPage}
          </a>
          <a
            href="#"
            className="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 border-t-2 pt-4 px-4 inline-flex items-center text-sm font-medium"
          >
            {props.currentPage + 1}
          </a>
          <span className="border-transparent text-gray-500 border-t-2 pt-4 px-4 inline-flex items-center text-sm font-medium">
            ...
          </span>
          <a
            href="#"
            className="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 border-t-2 pt-4 px-4 inline-flex items-center text-sm font-medium"
          >
            {props.lastPage - 1}
          </a>
          <a
            href="#"
            className="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 border-t-2 pt-4 px-4 inline-flex items-center text-sm font-medium"
          >
            {props.lastPage}
          </a>
        </div>
        <div className="-mt-px w-0 flex-1 flex justify-end">
          <a
            href="#"
            className="border-t-2 pr-20 border-transparent pt-4 pl-1 inline-flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300"
          >
            Next
            <ArrowNarrowRightIcon className="ml-3 h-5 w-5 text-gray-400" aria-hidden="true" />
          </a>
        </div>
      </nav>
    )
  }

export default Leaderboard;