import React from 'react';
import axios from 'axios';

function TutorialListing(props){
    return (
        <li className="bg-white shadow overflow-hidden px-4 py-4 sm:px-6 sm:rounded-md">
            <a href={"/tutorials/" + props.tutorial.id }>
                <div className="flex-1 grid grid-cols-4">
                    <div className="col-span-3">
                        <p className="text-2xl">{props.tutorial.title}</p>
                        <p>Learn how to deconstruct classes from within the code itself.</p>
                    </div>
                    <div className="col-span-1">
                        <span class="text-right inline-flex items-center px-2.5 py-0.5 rounded-md text-sm font-medium bg-blue-100 text-blue-800">01/12/2021</span>
                    </div>
                </div>
            </a>
        </li>
    )
}

class Library extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            newest: []
        }

        this.fetchRankings = this.fetchRankings.bind(this)
    }

    componentDidMount(){
        this.fetchRankings();
    }

    fetchRankings(){
        axios.get('http://localhost:8080/api/tutorials')
        .then(response => {
            // handle success
            console.log(response.data);
            this.setState({
                newest: response.data
            })
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    render(){

        const newest = this.state.newest.map((tutorial, index) => {
            return <TutorialListing tutorial={tutorial} key={tutorial.id}></TutorialListing>
        });

        return (
            <div>
                <div className="max-w-screen-xl mx-auto bg-white mt-4 rounded-lg border-black border-2">
                    <div className="max-w-screen-lg mx-auto mt-4 mb-4">
                        <h1 className="font-semibold text-5xl">Java Library</h1>
                        <p className="mt-4">Java is one of the world's most popular programming languages. It's an object-oreinted language that cleans up the faults and trickyness of C/C++. It was created with the motto, "Write once, run anywhere", achieved with its revolutionary Virtual Machine to allow seemingly seamless cross-platform capabilities. </p>
                    </div>
                </div>
                <div className="max-w-screen-lg mx-auto mt-5 grid grid-cols-5 justify-center gap-3">
                    <div className="bg-blue-600 hover:bg-blue-700 rounded-full py-1">
                        <p className="text-center text-white">General</p>
                    </div>
                    <div className="bg-blue-600 hover:bg-blue-700 rounded-full py-1">
                        <p className="text-center text-white">OOP</p>
                    </div>
                    <div className="bg-blue-600 hover:bg-blue-700 rounded-full py-1">
                        <p className="text-center text-white">Collections</p>
                    </div>
                    <div className="bg-blue-600 hover:bg-blue-700 rounded-full py-1">
                        <p className="text-center text-white">Streams</p>
                    </div>
                    <div className="bg-blue-600 hover:bg-blue-700 rounded-full py-1">
                        <p className="text-center text-white">Networking</p>
                    </div>
                    <div className="bg-blue-600 hover:bg-blue-700 rounded-full py-1">
                        <p className="text-center text-white">Concurrency</p>
                    </div>
                </div>
                <div className="mt-4 max-w-screen-xl mx-auto pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between">
                    <h3 className="text-lg leading-6 font-medium text-gray-900">Tutorials
                        <a href="/tutorials/new" type="button" class="ml-3 px-4 py-2 border border-transparent text-sm font-medium rounded-md text-green-900 bg-green-300 hover:bg-green-500 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500">
                            Create Tutorial
                        </a>
                    </h3>
                    
                    <div className="mt-3 sm:mt-0 sm:ml-4">
                        <label for="search_candidate" class="sr-only">Search</label>
                        <div className="flex rounded-md shadow-sm">
                            <div className="relative flex-grow focus-within:z-10">
                                <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                    <svg className="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd" />
                                    </svg>
                                </div>
                                <input type="text" name="search_candidate" id="search_candidate" className="focus:ring-indigo-500 focus:border-indigo-500 block w-full rounded-none rounded-l-md pl-10 sm:hidden border-gray-300" placeholder="Search" />
                                <input type="text" name="search_candidate" id="search_candidate" className="hidden focus:ring-indigo-500 focus:border-indigo-500 w-full rounded-none rounded-l-md pl-10 sm:block sm:text-sm border-gray-300" placeholder="Search candidates" />
                            </div>
                            <button type="button" className="-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-r-md text-gray-700 bg-gray-50 hover:bg-gray-100 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500">
                                <svg className="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path d="M3 3a1 1 0 000 2h11a1 1 0 100-2H3zM3 7a1 1 0 000 2h5a1 1 0 000-2H3zM3 11a1 1 0 100 2h4a1 1 0 100-2H3zM13 16a1 1 0 102 0v-5.586l1.293 1.293a1 1 0 001.414-1.414l-3-3a1 1 0 00-1.414 0l-3 3a1 1 0 101.414 1.414L13 10.414V16z" />
                                </svg>
                                <span className="ml-2">Sort</span>
                                <svg className="ml-2.5 -mr-1.5 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
                <div className="mx-auto max-w-screen-md mt-4">
                    <div className="bg-white shadow overflow-hidden sm:rounded-md">
                    <ul className="space-y-3">
                        { newest }
                    </ul>
                    </div>
                </div>
            </div>
        ) 
    }

}

export default Library;