import React from 'react';
import axios from 'axios';
import CortexLogo from '../images/CortexLogo.png';
import AuthService from '../AuthService.js';
import {Redirect} from 'react-router-dom';

import { connect } from 'react-redux';
import * as actions from '../store/reducers/actions/index.js';

class Login extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            errorMessage: '',
            email: '',
            password: '',
            isError: {
                'email': '',
                'password': '',
            },
            loginSuccess: false,
        }
        this.Auth = new AuthService();

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validate = this.validate.bind(this);
    }

    validate(){

        //validation
        let isError = { ...this.state.isError };

        let isValid = true;

        //validate email
        if (!this.state.email){
            isValid = false;
            isError['email'] = 'email not provided';
        }else{
            isError['email'] = '';
        }

        //validate password
        if (!this.state.password){
            isValid = false;
            isError['password'] = 'Password not provided';
        }else{
            isError['password'] = '';
        }

        this.setState({
            isError: isError
        });

        return isValid;
    }

    handleSubmit(event){

        event.preventDefault();

        if(this.validate()){
            this.props.login(this.state.email, this.state.password)
            .then(() => {
                console.log("Logged in!!!");
                this.setState({
                    loginSuccess: true,
                });
            })
            .catch(() => {
                console.log("Error logging in.");
            })
        }else{
            this.setState({errorMessage: 'Fix the issues below and try again.'});
        }

    }

    handleInputChange(event) {

        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
          [name]: value
        });
    }

    render(){

        if (this.state.loginSuccess) {
            // window.location.reload();
            return <Redirect to="/learn" />;
        }

        return (
            <div>
                <div class="flex flex-col justify-center py-6 sm:px-6 lg:px-8 pb-6">

                    <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-lg">
                        <div class="bg-white py-8 px-4 shadow sm:overflow-hidden rounded-lg sm:px-10">
                            <div class="sm:mx-auto sm:w-full sm:max-w-md">
                            <img class="animate-pulse mx-auto h-16 w-auto" src={ CortexLogo } alt="Workflow" />
                            <h2 class="mt-4 text-center text-4xl font-extrabold text-gray-900">Log in to the <span class="font">Cortex</span></h2>
                            <p class="mt-2 text-center text-lg text-gray-600 max-w">Or <a href="/register" class="font-medium text-blue-600 hover:text-blue-500">Register</a>
                            </p>
                        </div>

                            {
                                (this.state.errorMessage) ?
                                (
                                    <div className="mb-5 w-11/12 mx-auto">
                                        <span className="bg-red-300 text-red-700 text-center px-10 rounded-md py-2 ">
                                            {this.state.errorMessage}
                                        </span>
                                    </div>
                                ) : ''
                            }

                            <form onSubmit={this.handleSubmit} class="space-y-6" action="#" method="POST">
                                <div>
                                    <label for="email" class="block text-sm font-medium text-gray-700">Email</label>
                                    <div class="mt-1">
                                        <input onChange={this.handleInputChange} value={this.state.email} id="email" name="email" type="text" placeholder="kody@google.com" autocomplete="email" required className={this.state.isError['email'] ? "appearance-none block w-full px-3 py-2 border border-red-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm" : "appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"} />
                                        {!this.state.isError['email'] ? "" : <p class="mt-2 text-sm text-red-500" id="email-description">{this.state.isError['email']}</p>}
                                    </div>
                                </div>

                                <div>
                                    <label for="password" class="block text-sm font-medium text-gray-700">
                                        Password
                                    </label>
                                    <div class="mt-1">
                                        <input onChange={this.handleInputChange} id="password" name="password" type="password" autocomplete="current-password" required class={(!this.state.isError['password']) ? "appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" : "appearance-none block w-full px-3 py-2 border border-red-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"} />
                                        {!this.state.isError['password'] ? "" : <p class="mt-2 text-sm text-red-500" id="email-description">{this.state.isError['password']}</p>}
                                    </div>
                                </div>

                                <div>
                                    <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        ) 
    }

}

const mapStateToProps = state => {
    return {
        isAuthenticated: false
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // dispatching plain actions
        login: (email, password) => dispatch(actions.login(email, password)),
    }
}

export default connect(null, mapDispatchToProps)(Login);