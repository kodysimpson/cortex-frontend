import React from 'react';
import axios from 'axios';
import sanitizeHtml from 'sanitize-html';
import Tiptap from './Tiptap.js';
import { connect } from 'react-redux';
import {Redirect} from 'react-router-dom';
import hljs from 'highlight.js';

class NewTutorial extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            title: '',
            subtitle: '',
            libraryTag: 'General',
            resourceToAdd: null,
            resources: ['https://google.com', 'fwefewfewfewf'],
            data: '',
            user: this.props.user
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.createTutorial = this.createTutorial.bind(this);
        this.retrieveData = this.retrieveData.bind(this);
        this.addResource = this.addResource.bind(this);
        this.removeResource = this.removeResource.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
          [name]: value
        });
    }

    retrieveData (html) {
        this.setState({
            data: sanitizeHtml(html)
        })
    }

    addResource(event){
        this.setState({
            resources: this.state.resources.concat(this.state.resourceToAdd)
        })
    }

    removeResource(event){
        this.setState({
            resources: this.state.resources.pop()
        })
    }

    createTutorial(event){

        axios.post('http://localhost:8080/api/tutorials',
        {
            title: this.state.title,
            subtitle: this.state.subtitle,
            userId: this.state.user.id,
            libraryTag: this.state.libraryTag,
            articleHTML: this.state.data
        })
        .then(response => {
            // handle success
            
            console.log(response.data);

            this.props.history.push('/libraries');
            window.location.reload();

        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

        

    }

    render(){

        const resourcesList = this.state.resources.map((resource) => 
            <li>{resource}
                <button onClick={this.removeResource} type="button" class="mx-3 inline-flex items-center p-1 border border-transparent rounded-full shadow-sm text-white bg-gray-600 hover:bg-red-700 focus:outline-none">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                        <path fillRule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clipRule="evenodd" />
                    </svg>
                </button>
            </li>
            );

        return (
            <div className="max-w-screen-xl mx-auto bg-white mt-4 rounded-xl border-blue-900 border-4 py-4">
                <div className="max-w-screen-lg mx-auto mt-4 mb-4">
                    <h1 className="font-semibold text-blue-800 text-5xl font-sans pb-2">Write a new Tutorial ✍🏼</h1>
                    <div className="mt-4 mb-4 w-4/5 grid grid-cols-1 gap-y-4 gap-x-4 sm:grid-cols-6">
                        <div class="sm:col-span-4">
                            <label for="title" className="block text-sm font-medium text-gray-700">Title</label>
                            <div class="mt-1">
                                <input onChange={this.handleInputChange} type="text" name="title" id="title" className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md text-2xl" placeholder="How to code Minecraft" />
                            </div>
                        </div>
                        <div>
                            <label for="libraryTag" className="block text-sm font-medium text-gray-700">Library</label>
                            <select onChange={this.handleInputChange} id="libraryTag" name="libraryTag" className="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                                <option selected value="General">General</option>
                                <option value="Java">Java</option>
                                <option value="C++">C++</option>
                                <option value="Javascript">Javascript</option>
                            </select>
                        </div>
                        <div class="sm:col-span-4">
                            <label for="subtitle" className="block text-sm font-medium text-gray-700">Subtitle</label>
                            <div class="mt-1">
                                <input onChange={this.handleInputChange} type="text" name="subtitle" id="subtitle" className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md text-2xl" placeholder="This article will show you how to code Minecraft using Java" />
                            </div>
                        </div>
                        {/* <div>
                            <label for="library" className="block text-sm font-medium text-gray-700">Library</label>
                            <select onChange={this.handleInputChange} id="library" name="library" className="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                                <option selected value="General">General</option>
                                <option value="Java">Java</option>
                                <option value="C++">C++</option>
                                <option value="Javascript">Javascript</option>
                            </select>
                        </div> */}

                        
                    </div>
                    <div className="border-t-2 py-2 mx-auto w-3/4 grid grid-cols-2 gap-2 mb-2">
                        <p className="text-center text-lg font-semibold col-span-2">Supplemental Resources</p>
                        <div class="sm:col-span-1">
                            <label for="subtitle" className="block text-sm font-medium text-gray-700">Video Companion</label>
                            <div class="mt-1">
                                <input onChange={this.handleInputChange} type="text" name="subtitle" id="subtitle" className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md text-2xl" placeholder="Video link here" />
                            </div>
                        </div>
                        <div class="sm:col-span-1">
                            <label for="subtitle" className="block text-sm font-medium text-gray-700">Book Companion</label>
                            <div class="mt-1">
                                <input onChange={this.handleInputChange} type="text" name="subtitle" id="subtitle" className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md text-2xl" placeholder="Book link here" />
                            </div>
                        </div>
                        <div className="col-span-2 mx-auto ">

                            <div className="px-3 py-1">
                                <ul className="list-inside list-disc">
                                    {
                                        {resourcesList}
                                    }
                                </ul>
                            </div>

                            <div className="mt-2 w-2/4 mx-auto">
                                <label for="email" class="block text-sm font-medium text-gray-700">Extra Resource(s) <span className="text-gray-500">(Up to three)</span></label>
                                <div class="mt-1 flex rounded-md shadow-sm">
                                    <div class="relative flex items-stretch flex-grow focus-within:z-10">
                                    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                        <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M12.586 4.586a2 2 0 112.828 2.828l-3 3a2 2 0 01-2.828 0 1 1 0 00-1.414 1.414 4 4 0 005.656 0l3-3a4 4 0 00-5.656-5.656l-1.5 1.5a1 1 0 101.414 1.414l1.5-1.5zm-5 5a2 2 0 012.828 0 1 1 0 101.414-1.414 4 4 0 00-5.656 0l-3 3a4 4 0 105.656 5.656l1.5-1.5a1 1 0 10-1.414-1.414l-1.5 1.5a2 2 0 11-2.828-2.828l3-3z" clipRule="evenodd" />
                                        </svg>
                                    </div>
                                    <input onChange={(event) => {
                                        this.setState({
                                            resourceToAdd: event.target.value
                                        })
                                    }} type="text" name="resource" id="resource" class="focus:ring-blue-500 focus:border-blue-500 block rounded-none rounded-l-md pl-10 sm:text-sm border-gray-300 w-32" placeholder="Link here" />
                                    </div>
                                    <button onClick={this.addResource} class="-ml-px relative inline-flex items-center space-x-2 px-4 py-2 border border-gray-300 text-sm font-medium rounded-r-md text-gray-700 bg-gray-50 hover:bg-gray-100 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500">
                                        <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                            <path d="M3 3a1 1 0 000 2h11a1 1 0 100-2H3zM3 7a1 1 0 000 2h5a1 1 0 000-2H3zM3 11a1 1 0 100 2h4a1 1 0 100-2H3zM13 16a1 1 0 102 0v-5.586l1.293 1.293a1 1 0 001.414-1.414l-3-3a1 1 0 00-1.414 0l-3 3a1 1 0 101.414 1.414L13 10.414V16z" />
                                        </svg>
                                        <span>Add</span>
                                    </button>
                                </div>
                                </div>
                        </div>
                    </div>
                    <div>
                        <Tiptap retrieveData={this.retrieveData} />
                    </div>
                    <div className="text-right mt-5 space-x-3">
                        <button type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500">Go Back</button>
                        <button onClick={this.createTutorial} type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Post Tutorial</button>
                    </div>
                </div>
            </div>
        ) 
    }

}

export default NewTutorial;