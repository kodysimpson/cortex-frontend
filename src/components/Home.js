import React from 'react';
import axios from 'axios';

class Home extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            siteStats: {
                discordMembers: null,
                messagesSent: null,
                pointsEarned: null
            }
        }

    }

    componentDidMount(){

        axios.get('https://cortex-website-o3sik.ondigitalocean.app/api/stats')
        .then(response => {
            // handle success
            console.log(response.data);
            this.setState({
                siteStats: response.data
            })
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

    }

    render(){

        return (
            <div className="mb-20">
                <div class="py-8 flex flex-col justify-center">
                <div class="relative py-3 max-w-screen-xl mx-auto">
                    <div class="absolute inset-0 bg-gradient-to-r from-blue-600 to-green-700 shadow-lg transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"></div>
                    <div class="relative px-4 py-10 bg-white shadow-lg sm:rounded-3xl sm:p-20">
                    <div class="max-w-3xl mx-auto">
                        
                        <div>
                        <p class="text-center text-7xl font-semibold text-black">Kody Simpson</p>
                        <p class="text-center text-2xl text-gray-600 font-semibold p-5">The Hub for Developers</p>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                {/* <div className="mx-auto max-w-screen-md my-5">
                    <p className="font-bold text-4xl text-blue-800 mb-4 text-center">Recent Video</p>
                    <div className="flex content-center justify-center">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/MtNgTJo-FTE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div> */}
            </div>
            
        ) 
    }

}

export default Home;