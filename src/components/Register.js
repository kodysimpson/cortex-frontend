import React from 'react';
import axios from 'axios';
import CortexLogo from '../images/CortexLogo.png';
import AuthService from '../AuthService.js';

import { connect } from "react-redux";
import * as actions from '../store/reducers/actions/index.js';

class Register extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            success: false,
            errorMessage: '',
            email: '',
            password: '',
            passwordConfirm: '',
            name: '',
            motto: '',
            isError: {
                'email': '',
                'password': '',
                'name': ''
            }
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validate = this.validate.bind(this);
        this.register = this.register.bind(this);
    }

    register(){

        this.props.register(this.state.email, this.state.password, this.state.name, this.state.motto)
        .then((response) => {
          this.setState({
            success: true,
          });
        })
        .catch((error) => {
          this.setState({
            success: false,
          });
        });

        // axios.post('http://localhost:8080/api/auth/register',
        // {
        //     email: this.state.email,
        //     username: this.state.username,
        //     password: this.state.password,
        //     name: this.state.name,
        //     motto: this.state.motto
        // })
        // .then(response => {
        //     // handle success
            
        //     console.log(response.data);

        // })
        // .catch(error => {
        //     // handle error
        //     //console.log(error.response);

        //     if(error.response.status == 500){
        //         this.setState({errorMessage : error.response.data.message})
        //     }


        // });

    }

    validate(){

        //validation
        let isError = { ...this.state.isError };

        let isValid = true;

        //validate email
        if(!this.state.email){
            isValid = false;
            isError['email'] = 'No email provided';
        }else{
            isError['email'] = "";
        }

        //validate password
        if (!this.state.password || this.state.password.length < 8){
            isValid = false;
            isError['password'] = 'Password must be at least 8 characters';
        }else if (!this.state.passwordConfirm || (this.state.password !== this.state.passwordConfirm)){
            isValid = false;
            isError['password'] = 'Password and password confirmation must match';
        }else{
            isError['password'] = '';
        }

        //validate name
        if (!this.state.name || this.state.name.length < 2){
            isValid = false;
            isError['name'] = 'The name must be at least 2 characters';
        }else{
            isError['name'] = '';
        }

        this.setState({
            isError: isError
        });

        return isValid;
    }

    handleSubmit(event){

        event.preventDefault();

        if(this.validate()){
            this.register();
        }else{
            this.setState({errorMessage: 'Fix the issues below and try again.'});
        }

    }

    handleInputChange(event) {

        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
          [name]: value
        });
    }

    render(){

        return (
            <div>
                <div class="flex flex-col justify-center py-6 sm:px-6 lg:px-8 pb-6">
                    <div class="sm:mx-auto sm:w-full sm:max-w-md">
                        <img class="mx-auto h-12 w-auto" src={ CortexLogo } alt="Workflow" />
                        <h2 class="mt-6 text-center text-3xl font-extrabold text-gray-900">Account Registration</h2>
                        <p class="mt-2 text-center text-sm text-gray-600 max-w">Or <a href="#" class="font-medium text-indigo-600 hover:text-indigo-500">Log in</a>
                        </p>
                    </div>
                    <p>{(this.state.success) ? 'woo' : 'waaah'}</p>
                    <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
                        <div class="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">

                            {
                                (this.state.errorMessage) ?
                                (
                                    <div className="mb-5 w-11/12 mx-auto">
                                        <span className="bg-red-300 text-red-700 text-center px-10 rounded-md py-2 ">
                                            {this.state.errorMessage}
                                        </span>
                                    </div>
                                ) : ''
                            }

                            <form onSubmit={this.handleSubmit} class="space-y-6" action="#" method="POST">
                                <div>
                                    <label for="email" class="block text-sm font-medium text-gray-700">Email address</label>
                                    <div class="mt-1">
                                        <input onChange={this.handleInputChange} id="email" name="email" type="email" autocomplete="email" validate required class={(!this.state.isError['email']) ? "appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" : "appearance-none block w-full px-3 py-2 border border-red-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"} />
                                        {!this.state.isError['email'] ? "" : <p class="mt-2 text-sm text-red-500" id="email-description">{this.state.isError['email']}</p>}
                                    </div>
                                </div>

                                <div>
                                    <label for="password" class="block text-sm font-medium text-gray-700">
                                        Password
                                    </label>
                                    <div class="mt-1">
                                        <input onChange={this.handleInputChange} id="password" name="password" type="password" autocomplete="current-password" required class={(!this.state.isError['password']) ? "appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" : "appearance-none block w-full px-3 py-2 border border-red-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"} />
                                        {!this.state.isError['password'] ? "" : <p class="mt-2 text-sm text-red-500" id="email-description">{this.state.isError['password']}</p>}
                                    </div>
                                </div>
                                <div>
                                    <label for="passwordConfirm" class="block text-sm font-medium text-gray-700">
                                        Confirm Password
                                    </label>
                                    <div class="mt-1">
                                        <input onChange={this.handleInputChange} id="passwordConfirm" name="passwordConfirm" type="password" autocomplete="current-password" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
                                    </div>
                                </div>

                                <div class="relative">
                                    <div class="absolute inset-0 flex items-center">
                                        <div class="w-full border-t border-gray-300"></div>
                                    </div>
                                    <div class="relative flex justify-center text-sm">
                                        <span class="px-2 bg-white text-gray-500">About Yourself</span>
                                    </div>
                                </div>

                                <div>
                                    <label for="name" class="block text-sm font-medium text-gray-700">Name</label>
                                    <div class="mt-1">
                                        <input onChange={this.handleInputChange} id="name" name="name" type="text" placeholder="Thomas Jackson" required class={(!this.state.isError['name']) ? "appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" : "appearance-none block w-full px-3 py-2 border border-red-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm"} />
                                        {!this.state.isError['name'] ? <p class="mt-2 text-sm text-gray-500" id="email-description">This is how other users will see you.</p> : <p class="mt-2 text-sm text-red-500" id="email-description">{this.state.isError['name']}</p>}
                                    </div>
                                </div>

                                <div>
                                    <label for="motto" class="block text-sm font-medium text-gray-700">Motto</label>
                                    <div class="mt-1">
                                        <input onChange={this.handleInputChange} id="motto" name="motto" type="text" placeholder="Communism is Bad" class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" />
                                        <p class="mt-2 text-sm text-gray-500" id="email-description">A phrase you live by.</p>
                                    </div>
                                </div>

                                <div>
                                    <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        ) 
    }

}

function mapStateToProps(state) {
    const { message } = state.message;
    return {
      message,
    };
  }
  
  const mapDispatchToProps = dispatch => {
    return {
        // dispatching plain actions
        register: (email, password, name, motto) => dispatch(actions.register(email, password, name, motto)),
    }
}

  export default connect(null, mapDispatchToProps)(Register);