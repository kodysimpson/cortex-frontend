import React from 'react';
import axios from 'axios';
import RecentTutorials from './RecentTutorials';

class Libraries extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            newest: []
        }
    }

    render(){

        return (
            <div>
                <div className="max-w-screen-xl mx-auto bg-white mt-4 rounded-xl border-blue-800 border-4">
                    <div className="max-w-screen-lg mx-auto mt-4 mb-4">
                        <h1 className="font-semibold text-5xl">What are you going to learn today? 🤔</h1>
                        <p className="mt-4 text-2xl">This is your learning feed. You can see today's top posts, browse tutorials and learning paths, or fall asleep.</p>
                    </div>
                </div>
                <div>
                    <div className="py-6 flex space-x-2">
                        <p className="text-3xl">Recent Tutorials</p>
                        <a href="/tutorials/new" className="bg-blue-800 hover:bg-blue-900 text-white p-2 rounded-lg font-semibold flex space-x-1"><span>Write Tutorial</span>
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z" />
                            </svg>
                        </a>
                    </div>
                    <div>
                        <RecentTutorials></RecentTutorials>
                    </div>
                </div>
                {/* <div className="max-w-screen-xl mx-auto mt-5 grid grid-cols-4 space-x-3">
                    <div className="bg-indigo-700 rounded-xl h-100">
                        <a href="/libraries/java">
                            <img className="w-80" style={{height: 308 + "px"}} src="https://1.bp.blogspot.com/-rt84jXyy_XQ/UkJYnUQI75I/AAAAAAAAANg/riMowNDWLA0/s1600/java.png"></img>
                            <div className="text-white font-medium text-2xl text-center py-2">
                                Java
                            </div>
                        </a>
                    </div>
                    <div className="bg-indigo-700 rounded-xl">
                        <img className="w-80" style={{height: 308 + "px"}} src="https://cdn.freebiesupply.com/logos/large/2x/c-logo-png-transparent.png"></img>
                        <div className="text-white font-medium text-2xl text-center py-2">
                            C++
                        </div>
                    </div>
                    <div className="bg-indigo-700 rounded-xl">
                        <img className="w-80" style={{height: 308 + "px"}} src="https://i.imgur.com/igYbvzR.png"></img>
                        <div className="text-white font-medium text-2xl text-center py-2">
                            MC Plugins
                        </div>
                    </div>
                    <div className="bg-indigo-700 rounded-xl">
                        <img className="w-80" style={{height: 308 + "px"}} src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Python.svg/768px-Python.svg.png"></img>
                        <div className="text-white font-medium text-2xl text-center py-2">
                            Python
                        </div>
                    </div>
                </div> */}
            </div>
        ) 
    }

}

export default Libraries;