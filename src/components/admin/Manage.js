import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

function AccountListing(props){
    return (
        <tr>
            <td class="px-6 py-4 whitespace-nowrap">
                <div class="flex items-center">
                    <div class="flex-shrink-0 h-10 w-10">
                        <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=facearea&amp;facepad=4&amp;w=256&amp;h=256&amp;q=60" alt="" />
                    </div>
                    <div class="ml-4">
                        <div class="text-sm font-medium text-gray-900">{props.user.username}</div>
                        <div class="text-sm text-gray-500">{props.user.email}</div>
                    </div>
                </div>
            </td>
            <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900">{props.user.name}</div>
                <div class="text-sm text-gray-500">{props.user.motto}</div>
            </td>
            <td class="px-6 py-4 whitespace-nowrap">
                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">Active</span>
            </td>
            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">Admin</td>
            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                <a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
            </td>
        </tr>
    )
}

class Manage extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            users: []
        }

        this.getAccounts = this.getAccounts.bind(this);
    }

    componentWillMount(){
        this.getAccounts();
    }

    getAccounts(){

        axios.get('http://localhost:8080/api/auth/users')
            .then(response => {
                // handle success
                console.log(response.data);
                this.setState({
                    users: response.data
                })
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });

    }

    render(){

        const accountsList = this.state.users.map(user => {
            return <AccountListing user={user}></AccountListing>
        })

        return (
            <div>
                <header class="bg-white shadow">
                    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    <h1 class="text-3xl font-bold leading-tight text-gray-900">
                       Admin Dashboard
                    </h1>
                    </div>
                </header>
                <main className="mt-12">
                    <div class="flex flex-col">
                        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                    <table class="min-w-full divide-y divide-gray-200">
                                        <thead class="bg-gray-50">
                                            <tr>
                                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name </th>
                                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Title</th>
                                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Status</th>
                                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Role</th>
                                                <th scope="col" class="relative px-6 py-3">
                                                    <span class="sr-only">Edit</span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="bg-white divide-y divide-gray-200">
                                            {accountsList}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        ) 
    }

}

// const mapStateToProps = state => {
//     return {
//         creatorID: state.user.id
//     }
// }

export default connect(null)(Manage);