import React from 'react';
import axios from 'axios';
import { withRouter } from "react-router";
import {connect} from 'react-redux';

class Tutorial extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            tutorialId: '',
            title: '',
            subtitle: '',
            articleHTML: '',
            username: '',
            likes: []
        }

        this.getTutorial = this.getTutorial.bind(this);
    }

    componentWillMount(){
        this.getTutorial();
    }

    likeTutorial(){

        axios.post('http://localhost:8080/api/tutorials/like',
        {
            tutorialId: this.state.subtitle,
            userId: this.state.user.id,
        })
        .then(response => {
            // handle success
            
            console.log(response.data);

            // this.props.history.push('/libraries');
            // window.location.reload();

        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

    }

    getTutorial(){

        axios.get('http://localhost:8080/api/tutorials/' + this.props.match.params.id,
        {
            // auth: {
            //     username: 'gguser',
            //     password: 'AhTL7UBXsV37gRg5'
            // }
        })
        .then(response => {
            // handle success
            
            console.log(response);

            this.setState({
                tutorialId: response.data.id,
                title: response.data.title,
                subtitle: response.data.subtitle,
                articleHTML: response.data.articleHTML,
                username: response.data.user.username,
                libraryTag: response.data.libraryTag
            });

        })
        .catch(function (error) {
            // handle error
            console.log(error);
        });

    }

    render(){

        const stuff = this.state.articleHTML;

        return (
            <div className="grid grid-flow-col grid-cols-12 gap-x-2">
                <div className="col-span-9 bg-white mt-4 rounded-xl border-blue-800 border-4">
                    <div className="px-12 mt-4 mb-4">
                        <div className="space-y-4">
                            <h1 className="font-bold text-5xl">{this.state.title}</h1>
                            <h1 className="text-gray-600">{this.state.subtitle}</h1>
                        </div>
                        <div className="py-2 flex space-x-1">
                            <p className="text-lg">written by <span className="font-semibold text-blue-800">{this.state.username}</span> on June 19th</p>
                            <span className="inline-flex items-center px-2.5 py-0.5 rounded-md text-sm font-medium bg-green-100 text-green-800">
                                #{this.state.libraryTag}
                            </span>
                        </div>
                        <div class="relative max-w-screen-md mx-auto my-7">
                            <div class="absolute inset-0 flex items-center" aria-hidden="true">
                                <div class="w-full border-t border-gray-300"></div>
                            </div>
                            <div class="relative flex justify-center">
                                <span class="bg-white px-2 text-gray-500">
                                    2 min read
                                </span>
                            </div>
                        </div>
                        <div className="prose prose-xl prose-indigo" dangerouslySetInnerHTML={{ __html: stuff }}>
                        </div>

                    </div>
                </div>
                <div className="col-span-3">
                    <div className="px-5 mt-4 mb-4 bg-white shadow-lg rounded-lg py-2">
                        <div>
                            <h1 className="font-bold text-2xl">{'rgrgrgrgrg'} <span class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-blue-100 text-blue-800">
                            3 points
                            </span></h1>
                        </div>
                        <p className="italic text-gray-700">"{3}"</p>
                        <div class="relative max-w-screen-md mx-auto my-7">
                            <div class="absolute inset-0 flex items-center" aria-hidden="true">
                                <div class="w-full border-t border-gray-300"></div>
                            </div>
                            <div class="relative flex justify-center">
                                <span class="bg-white px-2 text-gray-500">
                                    <svg class="h-5 w-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                        <path fillRule="evenodd" d="M14.707 12.293a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 111.414-1.414L9 14.586V3a1 1 0 012 0v11.586l2.293-2.293a1 1 0 011.414 0z" clipRule="evenodd" />
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div className="w-full text-center space-x-1">
                            <button type="button" class="inline-flex items-center p-2 border border-transparent rounded-full shadow-sm text-white bg-pink-600 hover:bg-pink-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-pink-500">
                                <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z" />
                                </svg>
                            </button>
                            <button type="button" class="inline-flex items-center p-2 border border-transparent rounded-full shadow-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
                                <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                </svg>
                            </button>
                            <button type="button" class="inline-flex items-center p-2 border border-transparent rounded-full shadow-sm text-white bg-gray-600 hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500">
                                <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M10 14H5.236a2 2 0 01-1.789-2.894l3.5-7A2 2 0 018.736 3h4.018a2 2 0 01.485.06l3.76.94m-7 10v5a2 2 0 002 2h.096c.5 0 .905-.405.905-.904 0-.715.211-1.413.608-2.008L17 13V4m-7 10h2m5-10h2a2 2 0 012 2v6a2 2 0 01-2 2h-2.5" />
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div className="text-center">
                        {
                            (this.props.isAuthenticated && (this.props.role == "ROLE_ADMIN" || this.props.user.id == this.state.creatorID)) ? 
                            <button type="button" class="inline-flex items-center px-3.5 py-2 border border-transparent text-sm leading-4 font-medium rounded-full shadow-sm text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">
                            Delete Tutorial
                            </button>
                            : ''
                        }
                    </div>
                </div>
            </div>
            
        ) 
    }

}

const mapStateToProps = state => {
    return {
      isAuthenticated: (state.token !== null),
      role: state.role,
      user: state.user
    }
  }

export default withRouter(connect(mapStateToProps)(Tutorial));